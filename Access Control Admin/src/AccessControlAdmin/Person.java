/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package AccessControlAdmin;

/**
 *
 * @author Stephen
 */
public class Person {
    // Fields
    public int id;
    public int cardNo;
    public String firstName;
    public String lastName;
    
    // Constructor
    public Person(int initialId, int initialCardNo, String initialFirstName, String initialLastName) {
        id = initialId;
        cardNo = initialCardNo;
        firstName = initialFirstName;
        lastName = initialLastName;
    }
    
    public Person(int initialId, String initialFirstName, String initialLastName) {
        id = initialId;
        firstName = initialFirstName;
        lastName = initialLastName;
    }
}
