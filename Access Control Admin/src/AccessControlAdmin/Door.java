/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AccessControlAdmin;

import java.util.ArrayList;

/**
 *
 * @author Stephen
 */
public class Door {

    // Fields
    public int id;
    public String name;
    public boolean lockedState;
    public String accessLog;
    public boolean openState;

    // Constructors
    public Door(int initialId, String initialName, Boolean initialLockedState, String initialLog, Boolean initialOpenState) {
        id = initialId;
        name = initialName;
        lockedState = initialLockedState;
        accessLog = initialLog;
        openState = initialOpenState;
    }

    public Door(int initialId, String initialName) {
        id = initialId;
        name = initialName;
    }

    // Methods
    public void setName(String newName) {
        name = newName;
    }

    public void lockDoor() {
        lockedState = true;

    }

    public void unlockDoor() {
        lockedState = false;
    }

    // Returns the doors lock status as a string
    public String getLockedStateStr() {
        if (lockedState == true) {
            return "locked";
        } else {
            return "unlocked";
        }
    }

    // Returns the doors open state as a string
    public String getOpenStateStr() {
        if (openState == true) {
            return "Open";
        } else {
            return "Shut";
        }
    }
}
