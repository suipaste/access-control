/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package AccessControlAdmin;

import java.util.ArrayList;

/**
 *
 * @author Stephen
 */
public class AccessGroup {
    // Fields
    public int id;
    public String name;
    ArrayList<Door> agDoorList = new ArrayList<>();
    ArrayList<Person> agPersonList = new ArrayList<>();
    
    
    // Constructor
    public AccessGroup(int initialId, String initialName) {
        id = initialId;
        name = initialName;
    }
}


